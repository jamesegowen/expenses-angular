angular.module("expensesApp", ['ngRoute', 'chart.js'])
    .config(function($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "expense-list.html",
                controller: "ExpenseListController",
                resolve: {
                    expenses: function(Expenses) {
                        return Expenses.getExpenses();
                    }
                }
            })
            .when("/new/expense", {
                controller: "NewExpenseController",
                templateUrl: "expense-form.html"
            })
            .when("/expense/:expenseId", {
                controller: "EditExpenseController",
                templateUrl: "expense.html"
            })
            .when("/budgets", {
                controller: "BudgetsListController",
                templateUrl: "budget-list.html",
                resolve: {
                    budgets: function(Budgets) {
                        return Budgets.getBudgets();
                    }
                }
            })
            .when("/new/budget", {
                controller: "NewBudgetController",
                templateUrl: "budget-form.html"
            })
            .when("/budget/:budgetId", {
                controller: "EditBudgetController",
                templateUrl: "budget.html"
            })
            .when("/stats/", {
                controller: "PieCtrl",
                templateUrl: "stats.html",
                resolve: {
                    budgets: function(Budgets) {
                        return Budgets.getBudgets();
                    }
                }
            })
            .otherwise({
                redirectTo: "/"
            })
    })
    .service("Expenses", function($http) {
        this.getExpenses = function() {
            return $http.get("/expenses").
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error finding expenses.");
                });
        }
        this.createExpense = function(expense) {
            return $http.post("/expenses", expense).
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error creating expense.");
                });
        }
        this.getExpense = function(expenseId) {
            var url = "/expenses/" + expenseId;
            return $http.get(url).
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error finding this expense.");
                });
        }
        this.editExpense = function(expense) {
            var url = "/expenses/" + expense._id;
            return $http.put(url, expense).
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error editing this expense.");
                    console.log(response);
                });
        }
        this.deleteExpense = function(expenseId) {
            var url = "/expenses/" + expenseId;
            return $http.delete(url).
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error deleting this expense.");
                    console.log(response);
                });
        }
    })

    .service("Budgets", function($http) {
        this.getBudgets = function() {
            return $http.get("/budgets").
            then(function(response) {
                return response;
            }, function(response) {
                alert("Error finding budgets.");
            });
        }
        this.createBudget = function(budget) {
            return $http.post("/budgets", budget).
            then(function(response) {
                return response;
            }, function(response) {
                alert("Error creating budget.");
            });
        }
        this.getBudget = function(budgetId) {
            var url = "/budgets/" + budgetId;
            return $http.get(url).
            then(function(response) {
                return response;
            }, function(response) {
                alert("Error finding this budget.");
            });
        }
        this.editBudget = function(budget) {
            var url = "/budgets/" + budget._id;
            return $http.put(url, budget).
            then(function(response) {
                return response;
            }, function(response) {
                alert("Error editing this budget.");
                console.log(response);
            });
        }
        this.deleteBudget = function(budgetId) {
            var url = "/budgets/" + budgetId;
            return $http.delete(url).
            then(function(response) {
                return response;
            }, function(response) {
                alert("Error deleting this budget.");
                console.log(response);
            });
        }
    })
    .controller("ExpenseListController", function(expenses, $scope) {
        $scope.expenses = expenses.data;
    })
    .controller("NewExpenseController", function($scope, $location, Expenses) {
        $scope.back = function() {
            $location.path("#/");
        }

        $scope.saveExpense = function(expense) {
            Expenses.createExpense(expense).then(function(doc) {
                var expenseUrl = "/expense/" + doc.data._id;
                $location.path(expenseUrl);
            }, function(response) {
                alert(response);
            });
        }
    })
    .controller("EditExpenseController", function($scope, $routeParams, Expenses) {
        Expenses.getExpense($routeParams.expenseId).then(function(doc) {
            $scope.expense = doc.data;
        }, function(response) {
            alert(response);
        });

        $scope.toggleEdit = function() {
            $scope.editMode = true;
            $scope.expenseFormUrl = "expense-form.html";
        }

        $scope.back = function() {
            $scope.editMode = false;
            $scope.expenseFormUrl = "";
        }

        $scope.saveExpense = function(expense) {
            Expenses.editExpense(expense);
            $scope.editMode = false;
            $scope.expenseFormUrl = "";
        }

        $scope.deleteExpense = function(expenseId) {
            Expenses.deleteExpense(expenseId);
        }
    })

    .controller("BudgetsListController", function(budgets, $scope) {
        $scope.budgets = budgets.data;
    })
    .controller("NewBudgetController", function($scope, $location, Budgets) {
        $scope.back = function() {
            $location.path("#/budgets");
        }

        $scope.saveBudget = function(budget) {
            Budgets.createBudget(budget).then(function(doc) {
                var budgetUrl = "/budget/" + doc.data._id;
                $location.path(budgetUrl);
            }, function(response) {
                alert(response);
            });
        }
    })
    .controller("EditBudgetController", function($scope, $routeParams, Budgets) {
        Budgets.getBudget($routeParams.budgetId).then(function(doc) {
            $scope.budget = doc.data;
        }, function(response) {
            alert(response);
        });

        $scope.toggleEdit = function() {
            $scope.editMode = true;
            $scope.budgetFormUrl = "budget-form.html";
        }

        $scope.back = function() {
            $scope.editMode = false;
            $scope.budgetFormUrl = "/budgets";
        }

        $scope.saveBudget = function(budget) {
            Budgets.editBudget(budget);
            $scope.editMode = false;
            $scope.budgetFormUrl = "";
        }

        $scope.deleteBudget = function(budgetId) {
            Budgets.deleteBudget(budgetId);
        }
    })

    .controller("PieCtrl", function($scope,$routeParams,budgets){
        var arrCategories=[];
        var arrAmounts=[];

        for (var i=0; i<budgets.data.length;i++)
        {
            arrCategories.push(budgets.data[i].category);
            arrAmounts.push(budgets.data[i].remaining);
        }
        $scope.labels = arrCategories
        $scope.data = arrAmounts;
        $scope.options = {legend:{display : true},responsive:true};
        $scope.colours = [ '#e74c3c', '#3498db', '#f1c40f', '#2ecc71', '#e67e22', '#9b59b6', '#1abc9c'];
    });