var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
var dbUrl = "mongodb://jgowen:password@ds041566.mlab.com:41566/expenses-sept";
//test
var EXPENSES_COLLECTION = "expenses";
var BUDGETS_COLLECTION = "budgets";

var app = express();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

var db;

mongodb.MongoClient.connect(dbUrl, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  db = database;
  console.log("Database ready");

  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});

function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

app.get("/expenses", function(req, res) {
  db.collection(EXPENSES_COLLECTION).find({}).toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get expenses.");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.post("/expenses", function(req, res) {
  var newExpense = req.body;
  var negAmount = newExpense.amount *-1;
  newExpense.createDate = new Date();

  db.collection(BUDGETS_COLLECTION).update({category:newExpense.category},{$inc:{remaining:negAmount}}, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update remaining budget.");
    } else {
      //res.status(201).json(doc.ops[0]);
      db.collection(EXPENSES_COLLECTION).insertOne(newExpense, function(err, doc) {
        if (err) {
          handleError(res, err.message, "Failed to create new expense.");
        } else {
          res.status(201).json(doc.ops[0]);
        }
      })
    }
  })


});

app.get("/expenses/:id", function(req, res) {
  db.collection(EXPENSES_COLLECTION).findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get expenses");
    } else {
      res.status(200).json(doc);
    }
  });
});

app.put("/expenses/:id", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc._id;

  db.collection(EXPENSES_COLLECTION).updateOne({_id: new ObjectID(req.params.id)}, updateDoc, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update expenses");
    } else {
      res.status(204).end();
    }
  });
});

app.delete("/expenses/:id", function(req, res) {
  db.collection(EXPENSES_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete expenses");
    } else {
      res.status(204).end();
    }
  });
});




app.get("/budgets", function(req, res) {
  db.collection(BUDGETS_COLLECTION).find({}).toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get budgets.");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.post("/budgets", function(req, res) {
  var newBudget = req.body;
  newBudget.createDate = new Date();

  db.collection(BUDGETS_COLLECTION).insertOne(newBudget, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to create new budget.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});

app.get("/budgets/:id", function(req, res) {
  db.collection(BUDGETS_COLLECTION).findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get budgets");
    } else {
      res.status(200).json(doc);
    }
  });
});

app.put("/budgets/:id", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc._id;

  db.collection(BUDGETS_COLLECTION).updateOne({_id: new ObjectID(req.params.id)}, updateDoc, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update budgets");
    } else {
      res.status(204).end();
    }
  });
});

app.delete("/budgets/:id", function(req, res) {
  db.collection(BUDGETS_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete budgets");
    } else {
      res.status(204).end();
    }
  });
});
